<?php

	// Put your MailChimp API and List ID hehe
	$api_key = '58844fa591814444f8683ebb7f0ef76b-us5';
	$list_id = '575e70e8ce';

	// Let's start by including the MailChimp API wrapper
	include('./inc/MailChimp.php');
	// Then call/use the class
	use \DrewM\MailChimp\MailChimp;
	$MailChimp = new MailChimp($api_key);

	// Submit subscriber data to MailChimp
	// For parameters doc, refer to: http://developer.mailchimp.com/documentation/mailchimp/reference/lists/members/
	// For wrapper's doc, visit: https://github.com/drewm/mailchimp-api
	$result = $MailChimp->post("lists/$list_id/members", [
							'email_address' => $_POST["email"],
							'status'        => 'subscribed',
						]);

	if ($MailChimp->success()) {
		// Success message
		echo "<p class='c-state'>Merci, vous êtes bien inscrit(e)s à l'infolettre!</p>";
	} else {
		echo "<p class='c-state'>Une erreur s'est produite. Veuillez recommencer de nouveau</p>";
	}
?>
